using System;
using FifteenGame;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assert = NUnit.Framework.Assert;

namespace FifteenGameTest
{
    [TestClass]
    public class DecoratorTest : TestBase
    {
        public DecoratorTest()
        {
            game = new GameDecorator(0, 1, 2, 3, 4, 5, 6, 7, 8);
        }

        [TestMethod]
        public override void ConstructorTest()
        {
            Assert.IsNotNull(game);
        }

        [TestMethod]
        public override void ShiftTest()
        {
            var game2 = game.Shift(1);
            var game3 = game.Shift(1);
            Assert.AreSame(game2, game3);
            Assert.AreEqual(game2[0,0], 1);
        }
    }
}