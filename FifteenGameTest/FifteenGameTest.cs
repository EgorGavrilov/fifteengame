﻿using System;
using System.Net;
using FifteenGame;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assert = NUnit.Framework.Assert;

namespace FifteenGameTest
{
    [TestClass]
    public class FifteenGameTest : TestBase
    {
        public FifteenGameTest()
        {
            game = new Game(0, 1, 2, 3, 4, 5, 6, 7, 8);
        }

        [TestMethod]
        public override void ConstructorTest()
        {
            var game8 = new Game(1,2,3,4,5,6,7,8,0);
            var game15 = new Game(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,0);
            Assert.IsNotNull(game8);
            Assert.IsNotNull(game15);
        }

        [TestMethod]
        public override void ShiftTest()
        {
            game = new Game(0, 1, 2, 3, 4, 5, 6, 7, 8);
            game.Shift(1);
            Assert.AreEqual((0,0), game.GetLocation(1));
            Assert.Throws<InvalidOperationException>(() => game.Shift(5));
            Assert.DoesNotThrow(() => game.Shift(4));
            Assert.AreEqual((1,1), game.GetLocation(0));
            Assert.AreEqual(4, game[0,1]);
            Assert.Throws<InvalidOperationException>(() => game.Shift(0));
        }
    }
}
