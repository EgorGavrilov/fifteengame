using System;
using FifteenGame;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assert = NUnit.Framework.Assert;

namespace FifteenGameTest
{
    [TestClass]
    public class ImmutableTest : TestBase
    {
        public ImmutableTest()
        {
            game = new ImmutableGame(0, 1, 2, 3, 4, 5, 6, 7, 8);
        }

        [TestMethod]
        public override void ConstructorTest()
        {
            var game8 = new ImmutableGame(1, 2, 3, 4, 5, 6, 7, 8, 0);
            var game15 = new ImmutableGame(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0);
            Assert.IsNotNull(game8);
            Assert.IsNotNull(game15);
        }

        [TestMethod]
        public override void ShiftTest()
        {
            var game2 = game.Shift(1);
            Assert.AreNotSame(game, game2);
            Assert.Throws<InvalidOperationException>(() => game.Shift(4));
        }
    }
}