using FifteenGame;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assert = NUnit.Framework.Assert;

namespace FifteenGameTest
{
    [TestClass]
    public abstract class TestBase
    {
        protected IGame game;

        [TestMethod]
        public abstract void ConstructorTest();

        [TestMethod]
        public void IndexTest()
        {
            for (int i = 0; i < game.Size; i++)
                for (int j = 0; j < game.Size; j++)
                {
                    Assert.AreEqual(i*game.Size + j, game[i, j]);
                }
        }

        [TestMethod]
        public void GetLocationTest()
        { 
            for (int i = 0; i < game.Size; i++)
                for (int j = 0; j < game.Size; j++)
                    Assert.AreEqual((i, j), game.GetLocation(i*game.Size + j));
        }

        [TestMethod]
        public abstract void ShiftTest();
    }
}