using System;
using System.Linq;

namespace FifteenGame
{
    public class ImmutableGame : GameBase
    {
        public ImmutableGame(params int[] args) : base(args)
        {
        }

        public override IGame Shift(int value)
        {
            (int row, int column) location, zeroLocation;
            if (CanShift(value, out location, out zeroLocation))
            {
                var args = field.Cast<int>().ToArray();
                args[location.row * Size + location.column] = 0;
                args[zeroLocation.row * Size + zeroLocation.column] = value;
                return new ImmutableGame(args);
            }
            throw new InvalidOperationException();
        }
    }
}