﻿using System;
using System.Security.Cryptography.X509Certificates;

namespace FifteenGame
{
    public class Game : GameBase
    {
        public Game(params int[] args):base(args)
        {
        }

        public override IGame Shift(int value)
        {
            (int row, int column) location, zeroLocation;
            if (CanShift(value, out location, out zeroLocation))
            {
                field[location.row, location.column] = 0;
                field[zeroLocation.row, zeroLocation.column] = value;
                coodrinates[value] = zeroLocation;
                coodrinates[0] = location;
                return this;
            }
            else
            {
                throw new InvalidOperationException();
            }
        }
    }
}