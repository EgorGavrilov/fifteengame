﻿using System.Collections.Generic;
using System.Linq;

namespace FifteenGame
{
    public class GameDecorator : GameBase
    {
        private readonly Dictionary<(int[,], int), ImmutableGame> states = new Dictionary<(int[,], int), ImmutableGame>();

        public GameDecorator(params int[] args) : base(args) {}

        public override IGame Shift(int value)
        {
            if (states.ContainsKey((field, value)))
            {
                return states[(field, value)];
            }
            else
            {
                var game = new ImmutableGame(field.Cast<int>().ToArray());
                var newState = game.Shift(value);
                states[(field, value)] = newState as ImmutableGame;
                return newState;
            }
        }
    }
}