using System;
using System.Collections.Generic;
using System.Dynamic;

namespace FifteenGame
{
    public abstract class GameBase : IGame
    {
        protected readonly int[,] field;
        protected readonly int size;
        protected readonly Dictionary<int, (int, int)> coodrinates = new Dictionary<int, (int, int)>();

        public int Size => size;

        protected GameBase(params int[] args)
        {
            size = (int)Math.Sqrt(args.Length);
            field = new int[size, size];
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    field[i, j] = args[i * size + j];
                    coodrinates[i*size + j] = (i, j);
                }
            }
        }

        public int this[int row, int column] => field[row, column];

        public (int row, int column) GetLocation(int value)
        {
            return coodrinates[value];
        }

        protected bool CanShift(int value, out (int row, int column) location, out (int row, int column) zeroLocation)
        {
            location = GetLocation(value);
            zeroLocation = GetLocation(0);
            if ((zeroLocation.row == location.row && Math.Abs(zeroLocation.column - location.column) == 1 ||
                 zeroLocation.column == location.column && Math.Abs(zeroLocation.row - location.row) == 1) &&
                value != 0)
                return true;
            return false;
        }

        public abstract IGame Shift(int value);
    }
}