namespace FifteenGame
{
    public interface IGame
    {
        int Size { get; }
        int this[int row, int column] { get; }
        (int row, int column) GetLocation(int value);
        IGame Shift(int value);
    }
}